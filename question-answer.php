<?php
class Question {
    public $number;
    public $question;
    public $answers;
    private $correct_answer;

    public function __construct($number, $question, $answers, $correct_answer){
        $this->number = $number;
        $this->question = $question;
        $this->answers = $answers;
        $this->correct_answer = $correct_answer;
    }

    public function get_correct(){
        return $this->correct_answer;
    }
}

$questions_page_1 = array(
    new Question(1, "What is PHP?", array("PHP is an open-source programming language", "PHP is used to develop dynamic and interactive websites", " PHP is a server-side scripting language", "All"), 4),
    new Question(2, "Who is the father of PHP?", array("Drek Kolkevi", "Rasmus Lerdorf", "Willam Makepiece", " List Barely"), 2),
    new Question(3, "What does PHP stand for?", array("Preprocessor Home Page", "Pretext Hypertext Processor", "Hypertext Preprocessor", "Personal Hyper Processor"), 3),
    new Question(4, " Which of the following is the default file extension of PHP files?", array(".php", ".html", ".css", ".xml"), 1),
    new Question(5, "The star (orther than the sun) nearest to the earth is?", array("Pole star", "Alpha Centauri", "Vega", "None"), 1),
);


$questions_page_2 = array(
    new Question(6, "Which is the closest planet to the Sun?", array("Jupiter", "Mercury", "Neptune", "Earth"), 2),
    new Question(7, "Which is the farthest planet from the Sun ?", array("Mercury", "Saturn", "Neptune", "Mars"), 3),
    new Question(8, "Which is the smallest planet in the Solar System ?", array("Mercury", "Jupiter", "Venus", "Saturn"), 2),
    new Question(9, "The largest planet in the Solar System is :", array("Mercury", "Jupiter", "Venus", "Saturn"), 1),
    new Question(10, "Which among the following is Inner planet ?", array("Mercury", "Jupiter", "Venus", "Saturn"), 3),
);

$all_correct_answers = array();
foreach(array_merge($questions_page_1, $questions_page_2) as $question){
    $all_correct_answers["ques_".$question->number] = $question->get_correct();
}
?>
