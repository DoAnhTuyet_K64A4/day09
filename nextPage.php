<?php
session_start();
include 'question-answer.php';
$cookie_duration = strtotime('+30 days');

if(isset($_POST['next'])){
    foreach($_POST as $key => $value) {
        if ($key != 'next'){
           setcookie($key, $value, $cookie_duration);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="styles.css" />
    <form action="score.php" method="post">
    <title>Quizz</title>
</head>
<body>
    <form action="" method="post">
    <?php
        foreach($questions_page_2 as $ques){
            echo "
            <div class='container mt-sm-5 my-1'>
                <div class='question ml-sm-5 pl-sm-5 pt-2'>
                    <div class='py-2 h5'>
                        <b>Question $ques->number. $ques->question</b>
                    </div>
            ";
            echo "<div class='ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3' id='answers'>";
            for ($i=0 ; $i<=3 ; $i++){
                $q = $ques->answers[$i];
                $val = $i + 1;
                if (isset($_COOKIE['ques_'.$ques->number]) and $_COOKIE['ques_'.$ques->number] == $val) {
                    echo "
                        <label class='answers'> $q
                            <input type='radio' name='ques_$ques->number' value=$val checked>
                            <span class='checkmark'></span>
                        </label>
                    ";
                } else {
                    echo "
                            <label class='answers'> $q
                                <input type='radio' name='ques_$ques->number' value=$val >
                                <span class='checkmark'></span>
                            </label>
                    ";
                }
            }
            echo "
                    </div>
                </div>
            </div>
            ";
        }
        ?>

        <div class="d-flex justify-content-center" style="margin-top:24px; margin-bottom: 24px">
            <button type="submit" class="next-btn">Submit</button>
        </div>

    </form>
</body>
</html>
